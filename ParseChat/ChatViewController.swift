//
//  ChatViewController.swift
//  ParseChat
//
//  Created by Jinhua on 2/23/18.
//  Copyright © 2018 Jinhua Chen. All rights reserved.
//

import UIKit
import Parse

class ChatViewController: UIViewController, UITableViewDataSource, UITableViewDelegate   {

    @IBOutlet weak var chatMessageField: UITextField!
    @IBOutlet weak var messageTableView: UITableView!
    
    // Array to hold the data
    var messagesArray = [PFObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        messageTableView.delegate = self
        messageTableView.dataSource = self
        
        chatMessageField.placeholder = "Type a Message"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onSend(_ sender: Any) {
        
        // Create a PFObject
        let chatMessage = PFObject(className: "Message")
        
        // Set the Text key to the text of the chatMessageField
        chatMessage["text"] = chatMessageField.text ?? ""

        // Save the PFObject
        chatMessage.saveInBackground { (success, error) in
            if success {
                print("The message was saved!")
                self.chatMessageField.text = ""
                
                // Retrieve the latest messages and reload the table
                let query = PFQuery(className: "Message")
                query.order(byDescending: "createdAt")
                query.findObjectsInBackground {
                    (objects: [PFObject]?, error: Error?) -> Void in
                    if error == nil {
                        print("objects found")
                        
                        if let objects = objects {
                            self.messagesArray = objects
                        }
                    }
                }
                
                query.addDescendingOrder("createdAt")
                self.messageTableView.reloadData()
            } else if let error = error {
                print("Problem saving message: \(error.localizedDescription)")
            }
        }
    }
    
    // MARK: TableView Delegate Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messagesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Create a table cell
        let cell = messageTableView.dequeueReusableCell(withIdentifier: "ChatCell", for: indexPath) as! ChatCell
        
        // Customize the cell
        cell.textLabel?.text = messagesArray[indexPath.row]["text"] as? String
        
        // Return the cell
        return cell
    }
    

}
